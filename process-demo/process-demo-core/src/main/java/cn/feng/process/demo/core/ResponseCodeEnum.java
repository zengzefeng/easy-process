package cn.feng.process.demo.core;

import lombok.Getter;

/**
 * 错误码枚举类
 * <p>
 * 如果有必要，还需要考虑i18n
 *
 * @author zzf
 */
public enum ResponseCodeEnum {

    SUCCESS(200, "操作成功！"),
    FAIL(500, "操作失败！"),
    PARAM_ERROR(400, "参数错误！");

    @Getter
    private final int code;

    @Getter
    private final String desc;

    ResponseCodeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }


}
