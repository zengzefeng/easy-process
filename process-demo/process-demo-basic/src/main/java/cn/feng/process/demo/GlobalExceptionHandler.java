package cn.feng.process.demo;

import cn.feng.process.config.basic.base.ServiceException;
import cn.feng.process.config.basic.base.ServiceResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * 全局异常处理类
 *
 * @author zzf
 * @date 2020/7/6 9:47
 */
@ResponseBody
@ControllerAdvice
public class GlobalExceptionHandler {

    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(ServiceException.class)
    public ServiceResponse<?> handlerServiceException(ServiceException e) {
        return new ServiceResponse<>(e);
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(RuntimeException.class)
    public ServiceResponse<?> handlerException(RuntimeException e) {
        e.printStackTrace();
        return new ServiceResponse<>(e);
    }

}
