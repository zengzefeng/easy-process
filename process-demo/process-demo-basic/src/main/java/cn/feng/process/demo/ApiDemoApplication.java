package cn.feng.process.demo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@MapperScan("cn.feng.process.demo.mapper")
@ComponentScan({"cn.feng.process.config","cn.feng.process.core","cn.feng.process.demo"})
@SpringBootApplication(scanBasePackages = {"cn.feng.process.config","cn.feng.process.core","cn.feng.process.demo"})
public class ApiDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiDemoApplication.class, args);
    }

}
