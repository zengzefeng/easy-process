package cn.feng.process.demo.service;

import cn.feng.process.core.pojo.DO.Process;
import cn.feng.process.core.pojo.DTO.ProcessAddDTO;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 流程相关业务类
 *
 * @author zzf
 * @date 2020/9/14 14:52
 */
public interface ProcessService extends IService<Process> {

    Process add(ProcessAddDTO param);
}
