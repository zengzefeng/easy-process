package cn.feng.process.demo.mapper;

import cn.feng.process.core.pojo.DO.ProcessNode;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 流程节点相关数据库操作类
 *
 * @author zzf
 * @date 2020/9/14 15:05
 */
@Mapper
public interface ProcessNodeMapper extends BaseMapper<ProcessNode> {


}
