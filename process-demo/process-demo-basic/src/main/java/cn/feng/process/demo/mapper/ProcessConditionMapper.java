package cn.feng.process.demo.mapper;

import cn.feng.process.core.pojo.DO.ProcessCondition;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 流程条件相关数据库操作类
 *
 * @author zzf
 * @date 2020/9/14 15:05
 */
@Mapper
public interface ProcessConditionMapper extends BaseMapper<ProcessCondition> {

}
