package cn.feng.process.demo;

import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * swagger配置类
 *
 * @author zzf
 * @date 2020/6/29 14:38
 */
@EnableSwagger2
@Configuration
@ConfigurationProperties("swagger.config")
public class SwaggerConfig {

    @Setter
    private String title;

    @Setter
    private String description;


    @Bean
    public Docket createRestApi() {

        ProjectProperties projectProperties = new ProjectProperties();


        return new Docket(DocumentationType.SWAGGER_2)
                .groupName(projectProperties.serverName)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("cn.feng.process.demo"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(this.title)
                .description(this.description)
                .version("1.0")
                .build();
    }


    @Configuration
    public static class ProjectProperties {

        @Value("${spring.application.name}")
        public String serverName;


    }


}
