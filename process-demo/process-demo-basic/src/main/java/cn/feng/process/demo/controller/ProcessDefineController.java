package cn.feng.process.demo.controller;

import cn.feng.process.config.basic.ProcessConfig;
import cn.feng.process.config.basic.base.BaseController;
import cn.feng.process.config.basic.base.ServiceResponse;
import cn.feng.process.core.pojo.DO.ProcessDefine;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 流程定义相关接口
 *
 * @author zzf
 * @date 2020/9/14 13:45
 */
@Api("流程定义相关接口")
@RequestMapping("/define")
@RestController
public class ProcessDefineController extends BaseController {

    @ApiOperation("获取流程定义")
    @GetMapping("/list")
    public ServiceResponse<List<ProcessDefine>> list() {
        return success(ProcessConfig.I.getProcessDefineList());
    }

}
