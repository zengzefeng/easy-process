package cn.feng.process.demo.service;

import cn.feng.process.core.pojo.DO.ProcessExecute;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 流程执行相关业务类
 *
 * @author zzf
 * @date 2020/9/14 14:53
 */
public interface ProcessExecuteService extends IService<ProcessExecute> {


    boolean run(String targetName, long targetId, long userId) ;
}
