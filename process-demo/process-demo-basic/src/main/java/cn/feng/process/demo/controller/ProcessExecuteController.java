package cn.feng.process.demo.controller;

import cn.feng.process.config.basic.base.BaseController;
import cn.feng.process.config.basic.base.ServiceResponse;
import cn.feng.process.demo.service.ProcessExecuteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 流程相关接口
 *
 * @author zzf
 * @date 2020/9/14 13:45
 */
@Api("流程相关接口")
@RequestMapping("/execute")
@RestController
public class ProcessExecuteController extends BaseController {

    @Autowired
    ProcessExecuteService service;

    @ApiOperation("发起流程")
    @PostMapping("/run")
    public ServiceResponse<?> run(@ApiParam("对象名")
                                  @RequestParam String targetName,
                                  @ApiParam("对象id")
                                  @RequestParam long targetId,
                                  @ApiParam("发起人ID")
                                  @RequestParam long userId) {
        return success(service.run(targetName, targetId, userId));
    }

}
