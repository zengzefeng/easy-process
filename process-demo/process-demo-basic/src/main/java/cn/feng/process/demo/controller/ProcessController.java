package cn.feng.process.demo.controller;

import cn.feng.process.config.basic.base.BaseController;
import cn.feng.process.config.basic.base.ServiceResponse;
import cn.feng.process.demo.service.ProcessService;
import cn.feng.process.core.pojo.DO.Process;
import cn.feng.process.core.pojo.DTO.ProcessAddDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 流程相关接口
 *
 * @author zzf
 * @date 2020/9/14 13:45
 */
@Api("流程相关接口")
@RequestMapping("/define")
@RestController
public class ProcessController extends BaseController {

    @Autowired
    ProcessService service;

    @ApiOperation("添加流程")
    @PostMapping
    public ServiceResponse<Process> add(@RequestBody ProcessAddDTO param) {
        return success(service.add(param));
    }

}
