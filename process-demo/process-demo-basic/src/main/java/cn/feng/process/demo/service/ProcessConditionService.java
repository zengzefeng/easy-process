package cn.feng.process.demo.service;

import cn.feng.process.core.pojo.DO.ProcessCondition;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 流程条件相关业务类
 *
 * @author zzf
 * @date 2020/9/14 14:52
 */
public interface ProcessConditionService extends IService<ProcessCondition> {

}
