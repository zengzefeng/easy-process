package cn.feng.process.demo.service.impl;

import cn.feng.process.demo.mapper.ProcessConditionMapper;
import cn.feng.process.demo.service.ProcessConditionService;
import cn.feng.process.core.pojo.DO.ProcessCondition;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 流程条件相关业务实现类
 *
 * @author zzf
 * @date 2020/9/14 15:01
 */
@Service
public class ProcessConditionServiceImpl extends ServiceImpl<ProcessConditionMapper, ProcessCondition> implements ProcessConditionService {

}
