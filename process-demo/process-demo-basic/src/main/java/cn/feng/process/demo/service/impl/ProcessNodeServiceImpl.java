package cn.feng.process.demo.service.impl;

import cn.feng.process.demo.mapper.ProcessNodeMapper;
import cn.feng.process.demo.service.ProcessNodeService;
import cn.feng.process.core.pojo.DO.ProcessNode;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 流程节点相关业务实现类
 *
 * @author zzf
 * @date 2020/9/14 15:01
 */
@Service
public class ProcessNodeServiceImpl extends ServiceImpl<ProcessNodeMapper, ProcessNode> implements ProcessNodeService {

}
