package cn.feng.process.demo.service.impl;

import cn.feng.process.demo.mapper.ProcessMapper;
import cn.feng.process.demo.service.ProcessService;
import cn.feng.process.core.pojo.DO.Process;
import cn.feng.process.core.pojo.DTO.ProcessAddDTO;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

/**
 * 流程相关业务实现类
 *
 * @author zzf
 * @date 2020/9/14 15:01
 */
@Service
public class ProcessServiceImpl extends ServiceImpl<ProcessMapper, Process> implements ProcessService {

    @Override
    public Process add(ProcessAddDTO param) {
        Process process = new Process();
        BeanUtils.copyProperties(param, process);
        return null;
    }
}
