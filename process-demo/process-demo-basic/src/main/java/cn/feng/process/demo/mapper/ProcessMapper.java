package cn.feng.process.demo.mapper;

import cn.feng.process.core.pojo.DO.Process;
import cn.feng.process.core.pojo.DTO.ProcessAddDTO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 流程相关数据库操作类
 *
 * @author zzf
 * @date 2020/9/14 15:05
 */
@Mapper
public interface ProcessMapper extends BaseMapper<Process> {

    ProcessAddDTO selectAllByTargetNameAndId(String targetName, Long targetId);

}
