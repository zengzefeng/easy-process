package cn.feng.process.demo.mapper;

import cn.feng.process.core.pojo.DO.ProcessExecute;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

/**
 * 流程执行相关数据库操作类
 *
 * @author zzf
 * @date 2020/9/14 15:05
 */
@Mapper
public interface ProcessExecuteMapper extends BaseMapper<ProcessExecute> {

    Map<String,Object> selectBySql(String selectSql);
}
