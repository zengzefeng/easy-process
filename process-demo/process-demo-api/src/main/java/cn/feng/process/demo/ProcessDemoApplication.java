package cn.feng.process.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(scanBasePackages = "cn.feng.process")
public class ProcessDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProcessDemoApplication.class, args);
    }

}
