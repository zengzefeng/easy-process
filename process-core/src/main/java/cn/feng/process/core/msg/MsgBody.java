package cn.feng.process.core.msg;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 消息体
 *
 * @author zzf
 * @date 2020/9/14 16:04
 */
@Getter
@Setter
@ApiModel("流程消息体")
public class MsgBody {

    @ApiModelProperty("消息类型")
    private Integer type;

    @ApiModelProperty("消息内容")
    private String content;

    @ApiModelProperty("点击查看时触发事件")
    private String showTriggerEvent;
}
