package cn.feng.process.core.pojo.DO;

import cn.feng.process.core.BaseDO2;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@TableName("pub_process")
@ApiModel("流程定义实体类")
public class Process extends BaseDO2 {

    @ApiModelProperty("主键ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("流程名")
    private String name;

    @ApiModelProperty("绑定的对象类名")
    private String targetClassName;

}
