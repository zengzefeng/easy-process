package cn.feng.process.core;

import cn.feng.process.core.pojo.DO.ProcessDefine;
import cn.feng.process.core.msg.IMsgSender;

/**
 * 通用流程对象
 *
 * @author zzf
 */
public abstract class BaseProcess {

    private ProcessDefine processDefine;

    private IMsgSender msgSender;

    /**
     * 执行流程
     *
     * @return succeeded?
     */
    protected Boolean execute() {



        return true;
    }

    /**
     * 撤回流程
     *
     * @return succeeded?
     */
    protected Boolean recall() {


        return true;
    }



}
