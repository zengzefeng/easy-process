package cn.feng.process.core.pojo.DO;

import cn.feng.process.core.BaseDO;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 流程条件对象
 *
 * @author zzf
 * @since 2019-04-11
 */
@Getter
@Setter
@TableName("pub_process_condition")
@ApiModel("流程条件对象")
public class ProcessCondition extends BaseDO {

    @ApiModelProperty(value = "主键ID")
    @TableId(type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("流程id")
    private Integer processId;

    @ApiModelProperty("条件")
    @TableField(typeHandler = JacksonTypeHandler.class)
    private ProcessConditionDetailList conditionList;

    @ApiModelProperty("前置流程节点对象id")
    private Integer aheadProcessNodeId;

    @ApiModelProperty("后置流程节点对象id")
    private Integer hinderProcessNodeId;

}
