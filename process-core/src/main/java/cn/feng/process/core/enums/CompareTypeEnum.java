package cn.feng.process.core.enums;

/**
 * 比较类型枚举类
 */
public enum CompareTypeEnum {

    EQUALS(1, "等于"),
    LARGE(2, "大于"),
    LARGE_EQUALS(3, "大于等于"),
    LESS(4, "小于"),
    LESS_EQUALS(5, "小于等于"),
    CONTAIN(6, "包含"),
    MEMBER(7, "在...之中");
    int key;
    String value;

    CompareTypeEnum(int key, String value) {
        this.key = key;
        this.value = value;
    }

}
