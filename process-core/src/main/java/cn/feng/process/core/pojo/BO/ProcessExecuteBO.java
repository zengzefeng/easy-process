package cn.feng.process.core.pojo.BO;

import cn.feng.process.core.pojo.DO.ProcessExecute;
import cn.feng.process.core.pojo.DO.ProcessExecuteReply;
import cn.feng.process.core.pojo.DO.ProcessExecuteUser;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 流程执行业务对象
 *
 * @author zzf
 * @date 2020/9/14 17:05
 */
@Getter
@Setter
public class ProcessExecuteBO extends ProcessExecute {

    /**
     * 流程审核人列表
     */
    private List<ProcessExecuteUser> userList;

    /**
     * 流程回复信息列表
     */
    private List<ProcessExecuteReply> replyList;


}
