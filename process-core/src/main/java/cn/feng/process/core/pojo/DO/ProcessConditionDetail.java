package cn.feng.process.core.pojo.DO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 流程定义条件详情对象
 * <p>
 * 也可以直接声明成内部类，但是为了在引用是能直接使用类名，所以单独创建一个类。
 *
 * @author zzf
 */
@Getter
@Setter
@ApiModel("流程条件详情对象")
public class ProcessConditionDetail implements Serializable {

    @ApiModelProperty("涉及到的表对象 二选一：用户表、流程对象表名")
    private String targetName;

    @ApiModelProperty("涉及到的字段名")
    private String field;

    /**
     * 取值：{@link cn.feng.process.core.enums.CompareTypeEnum}
     */
    @ApiModelProperty("比较类型")
    private Integer compareType;

    @ApiModelProperty("比较的值（部分用数组的json字符串形式保存）")
    private String vals;
}
