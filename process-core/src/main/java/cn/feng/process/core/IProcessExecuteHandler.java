package cn.feng.process.core;

import cn.feng.process.core.pojo.BO.ProcessBO;

/**
 * 流程处理封装类
 *
 * @author zzf
 * @date 2020/9/14 17:03
 */
public interface IProcessExecuteHandler {

    ProcessBO getProcessBO();

}
