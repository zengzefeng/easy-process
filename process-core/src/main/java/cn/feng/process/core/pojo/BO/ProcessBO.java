package cn.feng.process.core.pojo.BO;

import cn.feng.process.core.pojo.DO.Process;
import cn.feng.process.core.pojo.DO.ProcessCondition;
import cn.feng.process.core.pojo.DO.ProcessNode;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 流程业务对象
 *
 * @author zzf
 * @date 2020/9/14 17:08
 */
@Getter
@Setter
public class ProcessBO extends Process {

    /**
     * 流程审核人列表
     */
    private List<ProcessCondition> conditionList;

    /**
     * 流程回复信息列表
     */
    private List<ProcessNode> nodeList;


}
