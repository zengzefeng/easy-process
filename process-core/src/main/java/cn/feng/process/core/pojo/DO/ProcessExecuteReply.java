package cn.feng.process.core.pojo.DO;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * 流程执行回复信息对象
 *
 * @author zzf
 */
@Getter
@Setter
@TableName("process_execute_reply")
@ApiModel("流程执行回复信息对象")
public class ProcessExecuteReply implements Serializable {

    @ApiModelProperty("主键ID")
    @TableId(type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("流程运行id")
    private Integer processRunId;

    @ApiModelProperty("流程运行节点用户表id")
    private Integer processExecuteUserId;

    @ApiModelProperty("回复人id")
    private Integer userId;

    @ApiModelProperty("审核结果 1：通过 2：不通过 3：已阅")
    private Integer examineResult;

    @ApiModelProperty("详情")
    private String description;

    @ApiModelProperty("回复时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    private Date createTime;

}
