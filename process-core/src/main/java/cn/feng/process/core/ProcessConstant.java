package cn.feng.process.core;

/**
 * 流程常量类
 *
 * @author zzf
 * @date 2020/9/11 11:42
 */
public class ProcessConstant {

    /**
     * 操作对象类型：取决于用户对象的字段值
     */
    public static final int OPERATION_TARGET_TYPE_USER_FIELD = 1;

    /**
     * 操作对象类型：取决于绑定对象的字段值
     */
    public static final int OPERATION_TARGET_TYPE_TARGET_FIELD = 2;

    /**
     * 操作对象类型：取决于触发事件的返回值
     */
    public static final int OPERATION_TARGET_TYPE_TRIGGER_EVENT = 3;

    /**
     * 流程执行状态：进行中
     */
    public static final Integer PROCESS_EXECUTE_STATUS_RUNNING = 1;

    /**
     * 流程执行状态：完成
     */
    public static final Integer PROCESS_EXECUTE_STATUS_FINISH = 2;

    /**
     * 流程执行状态：驳回
     */
    public static final Integer PROCESS_EXECUTE_STATUS_REJECT = 3;

    /**
     * 流程执行状态：撤回
     */
    public static final Integer PROCESS_EXECUTE_STATUS_WITHDRAW = 4;

    /**
     * 流程审核结果：通过
     */
    public static final Integer EXAMINE_RESULT_PASS = 1;

    /**
     * 流程审核结果：不通过
     */
    public static final Integer EXAMINE_RESULT_UN_PASS = 2;

    /**
     * 流程审核结果：已阅
     */
    public static final Integer EXAMINE_RESULT_READ = 3;


    /**
     * 默认 是
     */
    public static final Integer DEFAULT_YES = 1;
    /**
     * 默认 否
     */
    public static final Integer DEFAULT_NO = 0;
    /**
     * 默认开始结束节点ID
     */
    public static final Integer START_AND_END_NODE_ID = 0;

}
