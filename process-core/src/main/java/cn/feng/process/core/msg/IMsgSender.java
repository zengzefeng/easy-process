package cn.feng.process.core.msg;

/**
 * 消息发送定义
 *
 * @author zzf
 * @date 2020/9/14 15:59
 */
public interface IMsgSender {

    /**
     * 发送流程通过消息
     * @return succeeded?
     */
    Boolean sendFinishMsg(MsgBody body);


    /**
     * 发送驳回消息
     * @return succeeded?
     */
    Boolean sendRejectMsg(MsgBody body);

}
