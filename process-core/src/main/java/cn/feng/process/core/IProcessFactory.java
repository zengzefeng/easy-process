package cn.feng.process.core;

/**
 * 流程工厂
 *
 * @author ssf
 * @date 2020/9/14 15:13
 */
public interface IProcessFactory {

    BaseProcess getProcess(String className);

    BaseProcess getProcess(Class<?> clazz);

}
