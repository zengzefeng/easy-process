package cn.feng.process.core.pojo.DO;

import cn.feng.process.core.BaseDO2;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@TableName("process_execute")
@ApiModel("流程执行实体类")
public class ProcessExecute extends BaseDO2 {

    @ApiModelProperty("主键ID")
    @TableId(type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("流程id")
    private Integer processId;

    @ApiModelProperty("发起人id")
    private Integer runUserId;

    @ApiModelProperty("绑定的对象类名")
    private String targetClassName;

    @ApiModelProperty("对象id")
    private Integer targetId;

    @ApiModelProperty("当前运行的节点 用二进制数字表示 如：10000 表示总共5个节点，目前在第一个节点")
    private String runPoint;

    @ApiModelProperty(value = "流程状态 1运行中 2完成 3驳回 4撤回")
    private Integer runStatus;
}
