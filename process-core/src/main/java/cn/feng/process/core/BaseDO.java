package cn.feng.process.core;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * 实体对象通用父类
 * <p>
 * 定义成抽象类是为了解决Swagger文档中解析其子类时只能解析到该类的字段，无法解析到其子类信息的问题
 *
 * @author zzf
 */
@Setter
@Getter
public abstract class BaseDO implements Serializable {

    @ApiModelProperty("创建人")
    private Integer createBy;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @ApiModelProperty("创建时间")
    private Date createTime;

    @ApiModelProperty("修改人")
    private Integer updateBy;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @ApiModelProperty("修改时间")
    private Date updateTime;
}