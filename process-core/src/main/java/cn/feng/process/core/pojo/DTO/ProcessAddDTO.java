package cn.feng.process.core.pojo.DTO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 流程添加表单对象
 *
 * @author zzf
 * @date 2020/9/14 18:02
 */
@Getter
@Setter
@ApiModel("添加流程表单对象")
public class ProcessAddDTO {

    @ApiModelProperty("流程名")
    private String name;

    @ApiModelProperty("绑定的对象类名")
    private String targetClassName;

    @ApiModelProperty("节点集合")
    private List<NodeDTO> nodeDTOList;

    @ApiModelProperty("条件集合")
    private List<ConditionDTO> conditionDTOList;

    @Getter
    @Setter
    @ApiModel("流程节点表单对象")
    public static class NodeDTO {

        @ApiModelProperty("操作对象类型")
        private Integer operationTargetType;

        @ApiModelProperty("操作对象ID")
        private Integer operationTargetId;

        @ApiModelProperty("触发事件返回值")
        private String triggerEventResult;

        @ApiModelProperty("通过类型 1：所有人通过才算通过 2：一个人通过就可通过")
        private Integer passType;

        @ApiModelProperty("操作类型 1：知会 2：审核")
        private Integer operationType;
    }

    @Getter
    @Setter
    @ApiModel("流程节点表单对象")
    public static class ConditionDTO {

        @ApiModelProperty("操作对象ID")
        private Integer operationTargetId;

        @ApiModelProperty("操作对象类型 1:用户字段 2:绑定对象字段 3:触发事件")
        private Integer operationTargetType;

        @ApiModelProperty("操作对象对应的值")
        private String targetVal;

        @ApiModelProperty("通过类型 1：所有人通过才算通过 2：一个人通过就可通过")
        private Integer passType;

        @ApiModelProperty("操作类型 1：知会 2：审核")
        private Integer operationType;
    }


}
