package cn.feng.process.core;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 实体对象通用父类
 * <p>
 * 定义成抽象类是为了解决Swagger文档中解析其子类时只能解析到该类的字段，无法解析到其子类信息的问题
 *
 * @author zzf
 */
@Getter
@Setter
public abstract class BaseDO2 extends BaseDO implements Serializable {

    @ApiModelProperty("备注")
    private String description;

    @ApiModelProperty("是否删除 [0:未删 1:已删]")
    private Integer deleted;

    @ApiModelProperty("是否启用 [0:禁用 1:启用]")
    private Integer enabled;
}
