package cn.feng.process.core.enums;

/**
 * 流程配置类型枚举
 *
 * @author zzf
 * @date 2020/9/12 16:50
 */
public enum ProcessConfigTypeEnum {

    /**
     * 配置类配置
     */
    BASIC,

    /**
     * 配置中心-nacos配置
     */
    NACOS,

    /**
     * 配置中心-spring cloud config配置
     */
    SPRING_CLOUD_CONFIG,

    /**
     * 通过REDIS配置
     */
    REDIS,

    /**
     * 通过MySQL表数据配置
     */
    MYSQL;
}
