package cn.feng.process.core.pojo.DO;

import cn.feng.process.core.BaseDO;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 流程节点实体对象
 *
 * @author zzf
 */
@Getter
@Setter
@TableName("pub_process_node")
@ApiModel("流程节点实体对象")
public class ProcessNode extends BaseDO {

    @ApiModelProperty(value = "主键ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("流程id")
    private Integer processId;

    @ApiModelProperty("操作对象类型 1:用户字段 2:绑定对象字段 3:触发事件")
    private Integer operationTargetType;

    @ApiModelProperty("对应字段")
    private String targetField;

    @ApiModelProperty("对应的值")
    private String targetVal;

    @ApiModelProperty("通过类型 1：所有人通过才算通过 2：一个人通过就可通过")
    private Integer passType;

    @ApiModelProperty("操作类型 1：知会 2：审核 ")
    private Integer operationType;

}
