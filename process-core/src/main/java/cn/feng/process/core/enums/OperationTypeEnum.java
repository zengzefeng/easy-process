package cn.feng.process.core.enums;

/**
 * 流程操作类型枚举类
 *
 * @author zzf
 */
public enum OperationTypeEnum {

    /**
     * 知会，只能查看流程，不能对流程进行审核
     */
    INFORM(1, "知会"),

    /**
     * 审核，可以审核流程，通过/驳回
     */
    EXAMINE(2, "审核");

    private final int key;

    private final String value;

    public int getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    OperationTypeEnum(int key, String value) {
        this.key = key;
        this.value = value;
    }

}
