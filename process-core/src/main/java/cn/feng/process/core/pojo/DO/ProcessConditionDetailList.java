package cn.feng.process.core.pojo.DO;

import io.swagger.annotations.ApiModel;

import java.util.ArrayList;

/**
 * 流程定义条件详情对象集合
 * <p>
 * 创建这个是为了解决Mybatis解析{@link ProcessCondition} 的 conditions 字段时泛型擦除问题。
 * <p>
 * 也可以直接声明成内部类，但是为了在引用是能直接使用类名，所以单独创建一个类。
 *
 * @author zzf
 */
@ApiModel("流程条件详情对象集合")
public class ProcessConditionDetailList extends ArrayList<ProcessConditionDetail> {
}
