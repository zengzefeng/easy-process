package cn.feng.process.core.enums;

/**
 * 操作对象类型枚举
 *
 * @author zzf
 */
public enum OperationTargetTypeEnum {

    /**
     * 取决于用户对象的字段值
     */
    USER_FIELD(1),

    /**
     * 取决于绑定对象的字段值
     */
    TARGET_FIELD(2),

    /**
     * 取决于触发事件的返回值
     */
    TRIGGER_EVENT(3);

    private final int code;

    public int getCode() {
        return code;
    }

    OperationTargetTypeEnum(int code) {
        this.code = code;
    }

}
