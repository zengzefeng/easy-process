package cn.feng.process.core.pojo.DO;

import cn.feng.process.core.BaseDO2;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 流程执行审核人实体对象
 *
 * @author zzf
 */
@Getter
@Setter
@TableName("process_execute_user")
@ApiModel("流程执行审核人实体对象")
public class ProcessExecuteUser extends BaseDO2 {

    @ApiModelProperty("主键ID")
    @TableId(type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("流程执行id")
    private Integer processExecuteId;

    @ApiModelProperty("流程定义节点id")
    private Integer processNodeId;

    @ApiModelProperty("审核人id")
    private Integer userId;

    @ApiModelProperty("节点序号")
    private Integer orderNum;

}
