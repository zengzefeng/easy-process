package cn.feng.process.core.pojo.DO;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * 流程定义对象
 *
 * @author zzf
 */
@Getter
@Setter
@ApiModel("流程定义对象")
public class ProcessDefine implements Serializable {

    @ApiModelProperty("主键ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("名称")
    private String name;

    @ApiModelProperty("绑定的对象类名")
    private String targetClassName;

    @ApiModelProperty("绑定的对象的业务类类名")
    private String targetServiceClassName;

    @ApiModelProperty("操作对象定义类")
    private List<OperationTargetDefine> operationTargetDefineList;

    @ApiModelProperty("条件字段集合")
    private List<ConditionField> conditionFieldList;

    @ApiModelProperty("查看时的触发事件")
    private String showTriggerEvent;

    public Class<?> targetClass() throws ClassNotFoundException {
        return Class.forName(this.targetClassName);
    }


    @Getter
    @Setter
    @ApiModel("流程条件字段对象")
    public static class ConditionField implements Serializable {

        @ApiModelProperty("名称")
        private String name;

        @ApiModelProperty("数据类型 1数字 2时间 3字符")
        private Integer type;

        @ApiModelProperty("字段")
        private String field;

    }



}
