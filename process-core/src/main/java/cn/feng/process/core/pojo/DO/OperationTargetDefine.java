package cn.feng.process.core.pojo.DO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 流程操作对象设置
 *
 * @author zzf
 */

@Getter
@Setter
@ApiModel("流程操作对象设置实体类")
public class OperationTargetDefine implements Serializable {

    @ApiModelProperty("主键ID，可不重复")
    private Integer id;

    @ApiModelProperty("名称")
    private String name;

    /**
     * 取值见 {@link cn.feng.process.core.enums.OperationTargetTypeEnum}
     */
    @ApiModelProperty("操作对象类型")
    private Integer operationTargetType;

    @ApiModelProperty("对应的字段")
    private String targetField;

    /**
     * 当{@code (OperationTargetTypeEnum != TRIGGER_EVENT)}时，其值为对象字段
     * 否则为具体值，具体值可能是数组。
     */
    @ApiModelProperty("对应的值")
    private String targetValue;

    @ApiModelProperty("触发事件，仅当 operationTargetType = 3 时需要配置。")
    private String triggerEvent;
}
