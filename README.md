# easy-process

#### 介绍
一个简单通用的流程工具

#### 软件架构
软件架构说明

- easy-process
  - process-core `核心代码`
  
  - process-config `配置模块`
     - process-config-basic `通过配置文件配置` --doing
     - process-config-nacos `通过nacos配置`
     - process-config-scc `通过spring-cloud-config配置`
     - process-config-redis `通过redis配置`
     - process-config-mysql `通过mysql配置`
  - process-demo `示例`
     - process-demo-basic `通过配置文件配置示例`


#### 使用说明

`process-demo-basic`模块可以直接启动

swagger接口文档地址：
- 本地：[localhost:8080/doc.html](localhost:8080/doc.html)
- 远程：[https://easyprocess.utools.club/doc.html](https://easyprocess.utools.club/doc.html) 
(我本地提的服务，做的映射，连不上的话说明我服务关了。。)


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

