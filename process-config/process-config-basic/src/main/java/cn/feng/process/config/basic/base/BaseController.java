package cn.feng.process.config.basic.base;

/**
 * 接口父类
 *
 * @author zzf
 * @date 2020/6/29 15:41
 */
public abstract class BaseController {

    /**
     * 操作成功
     */
    public <T> ServiceResponse<T> success(T data) {
        return new ServiceResponse<>(data, ResponseCodeEnum.SUCCESS);
    }

    /**
     * 操作成功
     */
    public ServiceResponse<?> success() {
        return new ServiceResponse<>(null, ResponseCodeEnum.SUCCESS);
    }

    /**
     * 操作成功
     */
    public <T> ServiceResponse<T> success(String message, T data) {
        return new ServiceResponse<>(data, ResponseCodeEnum.SUCCESS.getCode(), message);
    }

    /**
     * 操作失败
     */
    public ServiceResponse fail(int code, String message) {
        return new ServiceResponse<>(code, message);
    }

    /**
     * 自定义返回前端失败状态
     */
    public ServiceResponse fail(String message) {
        return new ServiceResponse<>(ResponseCodeEnum.FAIL.getCode(), message);
    }

    /**
     * 参数错误
     */
    public ServiceResponse paramError() {
        return new ServiceResponse<>(ResponseCodeEnum.PARAM_ERROR);
    }

    /**
     * 参数错误
     */
    public ServiceResponse paramError(String message) {
        return new ServiceResponse<>(ResponseCodeEnum.PARAM_ERROR.getCode(), message);
    }


}
