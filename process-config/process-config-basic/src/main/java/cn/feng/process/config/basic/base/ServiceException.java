package cn.feng.process.config.basic.base;

/**
 * 业务异常类
 *
 * @author zzf
 */
public final class ServiceException extends RuntimeException {

    /**
     * 错误码
     */
    private final Integer code;

    public ServiceException(Integer code, String message) {
        super(message);
        this.code = code;
    }

    public ServiceException(String message) {
        super(message);
        this.code = 600;
    }

    public Integer getCode() {
        return code;
    }

}
